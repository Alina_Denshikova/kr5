import numpy as np
import sys

print("выберете метод")
met = int(input())

if met == 1:
    # -	Gauss-Jordan
    n = int(input('введите кол-во строк '))
    a = np.zeros((n, n + 1))
    x = np.zeros(n)

    print('введите аргументы матрицы')
    for i in range(n):
        for j in range(n + 1):
            a[i][j] = float(input('a[' + str(i) + '][' + str(j) + ']='))

    for i in range(n):
        if a[i][i] == 0.0:
            sys.exit('не может быть равна 0')

        for j in range(n):
            if i != j:
                ratio = a[j][i] / a[i][i]

                for k in range(n + 1):
                    a[j][k] = a[j][k] - ratio * a[i][k]

    for i in range(n):
        x[i] = a[i][n] / a[i][i]

    # Displaying solution
    print('\nрешение: ')
    for i in range(n):
        print('X%d = %0.2f' % (i, x[i]), end='\t')

if met == 2:
    # -	Cramer
    def cramer(mat, constant):

        D = np.linalg.det(mat)

        mat1 = np.array([constant, mat[:, 1], mat[:, 2]])
        mat2 = np.array([mat[:, 0], constant, mat[:, 2]])
        mat3 = np.array([mat[:, 0], mat[:, 1], constant])

        D1 = np.linalg.det(mat1)
        D2 = np.linalg.det(mat2)
        D3 = np.linalg.det(mat3)

        X1 = D1 / D
        X2 = D2 / D
        X3 = D3 / D

        print(X1, X2, X3)


    a = np.array([[10, 40, 70],
                  [20, 50, 80],
                  [30, 60, 80]])

    b = np.array([300, 360, 390])

    cramer(a, b)
